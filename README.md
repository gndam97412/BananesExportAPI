# BananesExportAPI

This project is an implementation of the [BananesExport Backend exercice](https://gitlab.com/aios-sh/BananesExport/-/tree/master/Back).

## Choices made

The project uses SpringBoot with Java 11, exposes a REST API and uses H2 in-memory database.

In order to test the application, Swagger has been implemented.

## Usage

Launch the SpringBoot application with your IDE, use a command line tool at the root of the project to launch it (mvn spring-boot:run -DskipTests or ./mvnw spring-boot:run -DskipTests on Windows) or build the JAR of the application.
Once the application is launched, you can use the SwaggerUI at http://localhost:8080/swagger-ui.html to try the application.

## License
[MIT](https://choosealicense.com/licenses/mit/)

