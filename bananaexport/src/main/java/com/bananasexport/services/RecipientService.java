package com.bananasexport.services;

import com.bananasexport.entities.Recipient;

import java.util.List;

public interface RecipientService {
    /**
     * Method to create a recipient
     * @param recipient the recipient to create
     * @return the created recipient
     */
    Recipient createRecipient(Recipient recipient) throws Exception;

    /**
     * Method to update a recipîent
     * @param recipientId the id of the recipient
     * @param recipient the recipient update
     * @return the updated recipient
     */
    Recipient updateRecipient(Long recipientId, Recipient recipient) throws Exception;

    /**
     * Method to delete a recipient
     * @param recipientId the recipient id
     */
    void deleteRecipient(Long recipientId) throws Exception;

    /**
     * Method to retrieve the list of known recipients
     * @return the lis of found recipients
     */
    List<Recipient> getRecipientList();
}
