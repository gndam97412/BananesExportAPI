package com.bananasexport.services.impl;

import com.bananasexport.entities.Order;
import com.bananasexport.entities.Recipient;
import com.bananasexport.repository.OrderRepository;
import com.bananasexport.repository.RecipientRepository;
import com.bananasexport.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.util.List;
import java.util.Objects;

@Service
public class OrderServiceImpl implements OrderService {

    private static final int BANANA_KILO_PRICE = 1;

    private OrderRepository orderRepository;
    private RecipientRepository recipientRepository;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository, RecipientRepository recipientRepository) {
        this.orderRepository = orderRepository;
        this.recipientRepository = recipientRepository;
    }

    /**
     * Method to create an order
     * @param order the order to create
     * @return the created order
     */
    @Override
    @Transactional
    public Order createOrder(Order order) throws Exception {

        if(Objects.isNull(order.getRecipient()) || (Objects.isNull(order.getRecipient().getId())) || !recipientRepository.existsById(order.getRecipient().getId())) {
            throw new Exception("The order cannot be created has the recipient does not exist");
        }
        setOrderPrice(order);
        return orderRepository.save(order);
    }

    /**
     * Method to update an order
     * @param orderId the order id
     * @param order the order to update
     * @return the updated order
     */
    @Override
    public Order updateOrder(Long orderId, Order order) throws Exception {

        Recipient orderRecipient = order.getRecipient();
        if(Objects.isNull(orderRecipient.getId()) || !recipientRepository.existsById(orderRecipient.getId())) {
            throw new Exception("The order cannot be updated has the recipient does not exist");
        }
        if(Objects.isNull(orderRecipient.getId()) || !orderRepository.existsById(orderId)) {
            throw new Exception("The order cannot be updated has it does not exist");
        }
        order.setId(orderId);
        setOrderPrice(order);
        return orderRepository.save(order);
    }

    /**
     * Method to delete an order
     * @param orderId the orderId
     */
    @Override
    public void deleteOrder(Long orderId) throws Exception {
        if(!orderRepository.existsById(orderId)) {
            throw new Exception(MessageFormat.format("The order of id {0} cannot be deleted", orderId));
        }
        orderRepository.deleteById(orderId);
    }

    /**
     * Method to get the list of orders of a recipient
     * @param recipientId the id of the recipient
     * @return the list of found orders
     */
    @Override
    public List<Order> getRecipientOrderList(Long recipientId) {
        return orderRepository.findByRecipient_Id(recipientId);
    }

    private void setOrderPrice(Order order) {
        order.setPrice(order.getQuantity() * BANANA_KILO_PRICE);
    }
}
