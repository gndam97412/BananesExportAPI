package com.bananasexport.services.impl;

import com.bananasexport.entities.Recipient;
import com.bananasexport.repository.RecipientRepository;
import com.bananasexport.services.RecipientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;

@Service
public class RecipientServiceImpl implements RecipientService {

    private RecipientRepository recipientRepository;

    @Autowired
    public RecipientServiceImpl(RecipientRepository recipientRepository) {
        this.recipientRepository = recipientRepository;
    }
    /**
     * Method to create a recipient
     * @param recipient the recipient to create
     * @return the created recipient
     */
    @Override
    public Recipient createRecipient(Recipient recipient) throws Exception {
        Optional<Recipient> existingRecipient = recipientRepository.findByNameIgnoreCaseAndAddressIgnoreCaseAndPostCodeIgnoreCaseAndCityIgnoreCaseAndCountryIgnoreCase(recipient.getName(),
                recipient.getAddress(), recipient.getPostCode(), recipient.getCity(), recipient.getCountry());
        if(existingRecipient.isEmpty()) {
            return recipientRepository.save(recipient);
        }
        throw new Exception("The recipient already exists");
    }

    /**
     * Method to update a recipîent
     * @param recipientId the id of the recipient
     * @param recipient the recipient update
     * @return the updated recipient
     */
    @Override
    public Recipient updateRecipient(Long recipientId, Recipient recipient) throws Exception {
        if(!recipientRepository.existsById(recipientId)) {
            throw new Exception(MessageFormat.format("The recipient of id {0} cannot be updated", recipientId));
        }
        recipient.setId(recipientId);

        return recipientRepository.save(recipient);
    }

    /**
     * Method to delete a recipient
     * @param recipientId the recipient id
     */
    @Override
    public void deleteRecipient(Long recipientId) throws Exception {
        if(!recipientRepository.existsById(recipientId)) {
            throw new Exception(MessageFormat.format("The recipient of id {0} cannot be deleted", recipientId));
        }
        recipientRepository.deleteById(recipientId);
    }

    /**
     * Method to retrieve the list of know recipient
     * @return the lis of found recipients
     */
    @Override
    public List<Recipient> getRecipientList() {
        return recipientRepository.findAll();
    }
}
