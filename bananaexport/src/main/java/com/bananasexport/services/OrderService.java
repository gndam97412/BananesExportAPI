package com.bananasexport.services;

import com.bananasexport.entities.Order;

import java.util.List;

public interface OrderService {
    /**
     * Method to create an order
     * @param order the order to create
     * @return the created order
     */
    Order createOrder(Order order) throws Exception;

    /**
     * Method to update an order
     * @param orderId the order id
     * @param order the order to update
     * @return the updated order
     */
    Order updateOrder(Long orderId, Order order) throws Exception;

    /**
     * Method to delete an order
     * @param orderId the orderId
     */
    void deleteOrder(Long orderId) throws Exception;

    /**
     * Method to get the list of orders of a recipient
     * @param recipientId the id of the recipient
     * @return the list of found orders
     */
    List<Order> getRecipientOrderList(Long recipientId);
}
