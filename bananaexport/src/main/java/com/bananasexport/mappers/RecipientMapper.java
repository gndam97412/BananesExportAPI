package com.bananasexport.mappers;

import com.bananasexport.dtos.RecipientDto;
import com.bananasexport.entities.Recipient;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RecipientMapper {

    RecipientDto toDto(Recipient recipient);

    List<RecipientDto> toDto(List<Recipient> recipientList);

    Recipient toRecipient(RecipientDto recipientDto);

}
