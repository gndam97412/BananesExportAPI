package com.bananasexport.mappers;

import com.bananasexport.dtos.OrderDto;
import com.bananasexport.entities.Order;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface OrderMapper {
    OrderDto toDto(Order order);

    List<OrderDto> toDto(List<Order> orderList);

    Order toOrder(OrderDto orderDto);
}
