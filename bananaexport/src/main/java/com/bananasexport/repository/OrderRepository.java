package com.bananasexport.repository;

import com.bananasexport.entities.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> findByRecipient_Id(Long recipientId);
}
