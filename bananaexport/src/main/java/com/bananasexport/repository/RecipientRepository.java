package com.bananasexport.repository;

import com.bananasexport.entities.Recipient;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RecipientRepository extends JpaRepository<Recipient, Long> {
    Optional<Recipient> findByNameIgnoreCaseAndAddressIgnoreCaseAndPostCodeIgnoreCaseAndCityIgnoreCaseAndCountryIgnoreCase(String name,
                                                                                                                           String address,
                                                                                                                           String postcode,
                                                                                                                           String city,
                                                                                                                           String country);
}
