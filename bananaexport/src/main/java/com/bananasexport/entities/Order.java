package com.bananasexport.entities;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "ORDERS")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", nullable = false)
    private Long id;
    @NonNull
    @Temporal(TemporalType.DATE)
    @Column(name = "DeliveryDate", nullable = false)
    private Date deliveryDate;
    @NonNull
    @Column(name = "Quantity", nullable = false)
    private int quantity;
    @Column(name = "Price", nullable = false)
    private int price;
    @ManyToOne(cascade=CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "RecipientId")
    private Recipient recipient;
}
