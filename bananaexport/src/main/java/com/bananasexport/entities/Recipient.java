package com.bananasexport.entities;

import lombok.*;

import javax.persistence.*;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "RECIPIENT")
public class Recipient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", nullable = false)
    private Long id;
    @NonNull
    @Column(name = "Name", nullable = false)
    private String name;
    @NonNull
    @Column(name = "Address", nullable = false)
    private String address;
    @NonNull
    @Column(name = "PostCode", nullable = false)
    private String postCode;
    @NonNull
    @Column(name = "City", nullable = false)
    private String city;
    @NonNull
    @Column(name = "Country", nullable = false)
    private String country;
}
