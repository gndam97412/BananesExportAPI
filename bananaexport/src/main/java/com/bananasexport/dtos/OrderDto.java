package com.bananasexport.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.Future;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Date;

@Data
@RequiredArgsConstructor
public class OrderDto {
    @JsonProperty("id")
    private Long id;
    @NonNull
    @JsonProperty("deliveryDate")
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date deliveryDate;
    @NonNull
    @Min(0)
    @Max(10000)
    @JsonProperty("quantity")
    private int quantity;
    @JsonProperty("price")
    private int price;
    @JsonProperty("recipient")
    @NonNull
    private RecipientDto recipient;
}
