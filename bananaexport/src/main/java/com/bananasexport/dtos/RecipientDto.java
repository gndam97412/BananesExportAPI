package com.bananasexport.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.constraints.NotBlank;

@Data
@RequiredArgsConstructor
public class RecipientDto {
    @JsonProperty("id")
    private Long id;
    @NotBlank
    @JsonProperty("name")
    private String name;
    @NotBlank
    @JsonProperty("address")
    private String address;
    @NotBlank
    @JsonProperty("postCode")
    private String postCode;
    @NotBlank
    @JsonProperty("city")
    private String city;
    @NotBlank
    @JsonProperty("country")
    private String country;
}
