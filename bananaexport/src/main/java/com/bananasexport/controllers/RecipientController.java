package com.bananasexport.controllers;

import com.bananasexport.dtos.RecipientDto;
import com.bananasexport.entities.Recipient;
import com.bananasexport.mappers.RecipientMapper;
import com.bananasexport.services.RecipientService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.ResponseEntity.status;

@RequestMapping(value = "/recipients")
@RestController
public class RecipientController {

    private final RecipientService recipientService;
    private final RecipientMapper recipientMapper;

    @Autowired
    public RecipientController(RecipientService recipientService, RecipientMapper recipientMapper) {
        this.recipientService = recipientService;
        this.recipientMapper = recipientMapper;
    }

    @ApiOperation(value = "getRecipientList", notes = "Endpoint to get the list of known recipients.", nickname = "getRecipientList")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "The list of know recipients has been successfully retrieved.", response = RecipientDto.class),
            @ApiResponse(code = 500, message = "An unexpected error has occurred.")
    })
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getRecipientList() {
        return new ResponseEntity<>(recipientMapper.toDto(recipientService.getRecipientList()), HttpStatus.CREATED);
    }

    @ApiOperation(value = "createRecipient", notes = "Endpoint to create a recipient.", nickname = "createRecipient")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Recipient created successfully.", response = RecipientDto.class),
            @ApiResponse(code = 404, message = "Cannot create the recipient."),
            @ApiResponse(code = 500, message = "An unexpected error has occurred.")
    })
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity createRecipient(@RequestBody @ApiParam(value = "The recipient to create.") @Valid RecipientDto recipient) {
        Recipient newRecipient = recipientMapper.toRecipient(recipient);
        try {
            return new ResponseEntity<>(recipientMapper.toDto(recipientService.createRecipient(newRecipient)), HttpStatus.CREATED);
        } catch (Exception e) {
            return status(HttpStatus.NOT_FOUND)
                    .body("Cannot create the recipient");
        }
    }

    @ApiOperation(value = "updateRecipient", notes = "Endpoint to update a recipient.", nickname = "updateRecipient")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recipient updated successfully.", response = RecipientDto.class),
            @ApiResponse(code = 400, message = "The recipient could not be updated."),
            @ApiResponse(code = 500, message = "An unexpected error has occurred.")
    })
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity updateRecipient(@PathVariable("id") @ApiParam(value = "The id of the recipient to update.") Long id,
                                          @RequestBody @ApiParam(value = "The recipient update object.") @Valid RecipientDto recipient) {
        Recipient updateRecipient = recipientMapper.toRecipient(recipient);
        try {
            return new ResponseEntity<>(recipientMapper.toDto(recipientService.updateRecipient(id, updateRecipient)), HttpStatus.OK);
        } catch (Exception e) {
            return status(HttpStatus.BAD_REQUEST)
                    .body("The recipient could not be updated");
        }
    }

    @ApiOperation(value = "deleteRecipient", notes = "Endpoint to delete a recipient.", nickname = "updateRecipient")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The recipient has been successfully deleted."),
            @ApiResponse(code = 404, message = "The recipient has not been found."),
            @ApiResponse(code = 500, message = "An unexpected error has occurred.")
    })
    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity deleteRecipient(@PathVariable("id") @ApiParam(value = "The id of the recipient to delete.") Long id) {

        try {
            recipientService.deleteRecipient(id);
            return status(HttpStatus.OK).body("The recipient has been successfully deleted");
        } catch (Exception e) {
            return status(HttpStatus.NOT_FOUND)
                    .body(e.getMessage());
        }
    }
}
