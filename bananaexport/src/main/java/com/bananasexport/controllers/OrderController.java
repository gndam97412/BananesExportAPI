package com.bananasexport.controllers;

import com.bananasexport.dtos.OrderDto;
import com.bananasexport.dtos.RecipientDto;
import com.bananasexport.entities.Order;
import com.bananasexport.mappers.OrderMapper;
import com.bananasexport.services.OrderService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static org.springframework.http.ResponseEntity.status;

@RequestMapping(value = "/orders")
@RestController
public class OrderController {

    private OrderService orderService;
    private OrderMapper orderMapper;

    @Autowired
    public OrderController(OrderService orderService, OrderMapper orderMapper) {
        this.orderService = orderService;
        this.orderMapper = orderMapper;
    }

    @ApiOperation(value = "getRecipientOrderList", notes = "Endpoint to get the list of orders of a recipient.", nickname = "getRecipientOrderList")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recipient order list successfully retrieved.", response = OrderDto.class),
            @ApiResponse(code = 500, message = "An unexpected error has occurred.")
    })
    @RequestMapping(method = RequestMethod.GET, value = "/recipient/{recipientId}")
    public ResponseEntity getRecipientOrderList(@PathVariable("recipientId") @ApiParam(value = "The recipient id.") Long recipientId) {
        return new ResponseEntity<>(orderMapper.toDto(orderService.getRecipientOrderList(recipientId)), HttpStatus.OK);
    }

    @ApiOperation(value = "createOrder", notes = "Endpoint to create an order.", nickname = "createOrder")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Order successfully created.", response = RecipientDto.class),
            @ApiResponse(code = 400, message = "Bad request."),
            @ApiResponse(code = 500, message = "An unexpected error has occurred.")
    })
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity createOrder(@RequestBody @ApiParam(value = "The order to create.") @Valid OrderDto order) {

        Order newOrder = orderMapper.toOrder(order);
        LocalDate today = LocalDate.now();
        LocalDate orderDate = convertToLocalDateViaInstant(newOrder.getDeliveryDate());

        if (orderDate.plusDays(7).isBefore(today)) {
            return status(HttpStatus.BAD_REQUEST)
                    .body("The delivery date should be at least 7 days from today.");
        }

        if (newOrder.getQuantity() % 25 != 0) {
            return status(HttpStatus.BAD_REQUEST)
                    .body("The quantity should be a multiple of 25.");
        }
        try {
            return new ResponseEntity<>(orderMapper.toDto(orderService.createOrder(newOrder)), HttpStatus.CREATED);
        } catch (Exception e) {
            return status(HttpStatus.BAD_REQUEST)
                    .body("The order cannot be created.");
        }
    }

    @ApiOperation(value = "updateOrder", notes = "Endpoint to update an order.", nickname = "updateOrder")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Order updated successfully.", response = RecipientDto.class),
            @ApiResponse(code = 400, message = "Bad request."),
            @ApiResponse(code = 500, message = "An unexpected error has occurred.")
    })
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")

    public ResponseEntity updateOrder(@PathVariable(name = "id") @ApiParam(value = "The order id.") Long id,
                                      @RequestBody @ApiParam(value = "The order update.") @Valid OrderDto order) {
        Order update = orderMapper.toOrder(order);
        LocalDate today = LocalDate.now();
        LocalDate orderDate = convertToLocalDateViaInstant(update.getDeliveryDate());

        if (orderDate.plusDays(7).isBefore(today)) {
            return status(HttpStatus.BAD_REQUEST)
                    .body("The delivery date should be at least 7 days from today.");
        }

        if (update.getQuantity() % 25 != 0) {
            return status(HttpStatus.BAD_REQUEST)
                    .body("The quantity should be a multiple of 25.");
        }

        try {
            return new ResponseEntity<>(orderMapper.toDto(orderService.updateOrder(id, update)), HttpStatus.OK);
        } catch (Exception e) {
            return status(HttpStatus.BAD_REQUEST)
                    .body("The order cannot be updated.");
        }
    }

    @ApiOperation(value = "deleteOrder", notes = "Endpoint to delete an order.", nickname = "deleteOrder")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Order successfully deleted."),
            @ApiResponse(code = 404, message = "Order not found."),
            @ApiResponse(code = 500, message = "An unexpected error has occurred.")
    })
    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity deleteOrder(@PathVariable("id") @ApiParam(value = "The order to delete id.") Long id) {

        try {
            orderService.deleteOrder(id);
            return status(HttpStatus.OK).body("The order has been successfully deleted");
        } catch (Exception e) {
            return status(HttpStatus.NOT_FOUND)
                    .body(e.getMessage());
        }
    }

    private LocalDate convertToLocalDateViaInstant(Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }
}
